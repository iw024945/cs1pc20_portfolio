#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define COM_SIZ 60
#define ARG_SIZ 1024
#define RES_SIZ 1024

int main(int argc, char *argv[]){
FILE *fp;
FILE *tests;
char command[COM_SIZ];
char arguments[ARG_SIZ];
char expected[RES_SIZ];
char actual[RES_SIZ];
char command_line[COM_SIZ + ARG_SIZ];
tests=fopen(argv[1], "rt");
if (tests==NULL){
printf("error opening file %s\n",argv[1]);
return 1;
}
while (fgets(command, COM_SIZ, tests)!= NULL){
fgets(arguments,ARG_SIZ,tests);
fgets(expected,RES_SIZ,tests);
strtok(command,"\n");
snprintf(command_line,sizeof command_line,"%s %s",command,arguments);


fp =popen(command_line,"r");
if (fp==NULL){
printf("failed to run command\n");
exit(1);
}
char message[RES_SIZ + RES_SIZ +21];

while(fgets(actual, sizeof(actual),fp)!=NULL){
snprintf(message,sizeof message,"%s %s %s %s","Expected",expected,"and got",actual);
printf("%s",message);
if(!strcmp(actual,expected)){
printf("OK\n");
}
else{printf("FAILED\n");}


pclose(fp);
}
fclose(tests);
}
return 0;
}












$ cd ~: Changes the current directory to the home directory.
2. $ cd portfolio: Changes the current directory to the "portfolio" directory.
3. $ git switch master: Switches to the "master" branch in the Git repository.
4. $ mkdir -p week4/framework: Creates a directory structure with a "framework" folder inside the "week4" folder.
5. $ cd week4/framework: Changes the current directory to "week4/framework".
6. $ git branch framework: Creates a new branch named "framework".
7. $ git switch framework: Switches to the "framework" branch.
8. $ nano Makefile: Opens the "Makefile" for editing using the nan text editor.
9. $ cat -vTE Makefile: Displays the content of the "Makefile" with tabs and end-of-line characters visible.
10.$ make feature NAME=test_output: Executes the "feature" goal in the Makefile with the variable NAME set to "test_output".
11.$ ls -al test_output: Lists detailed information about the contents of the "test_output" directory.
12.$ git add Makefile: Stages the changes made to the Makefile for the next commit.
13.$ git commit -m "Setting up Makefile to create feature folders": Commits the changes to the repository with a descriptive message.
14.$ git push: Pushes the committed changes to the remote repository.
15.$ cd test_output; cd src: Changes to the "test_output/src" directory.
16. (Commands related to creating and compiling "test_outputs.c".)
17.$ ./test_outputs file_does_not_exist: Attempts to run the "test_outputs" program with a non-existing file.
18.$ ./test_outputs: Runs the "test_outputs" program.
19. (Commands related to creating "op_test" file.)
20.$ ./test_outputs op_test: Runs the "test_outputs" program with the "op_test" file as input.
21.$ git add test_outputs.c: Stages the changes made to the "test_outputs.c" file for the next commit.
22.$ git add op_test: Stages the changes made to the "op_test" file for the next commit.
23.$ git commit -m "test framework and sample test suite": Commits the changes to the repository with a descriptive message.
24.$ git push: Pushes the committed changes to the remote repository.

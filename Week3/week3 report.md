1.	Cd ~ -- ensures we are in the master pathway
2.	Cd portfolio – traverse to the portfolio file
3.	Mkdir week3 – within the portfolio create the week 3 folder
4.	Mkdir week3/greeting – create a file in week 3 called greeting
5.	Cd week3/greeting – traverse into the greeting folder
6.	Git branch greeting – creates a new branch called greeting
7.	Git switch greeting – switch over to the greeting branch
8.	gcc -Wall -pedantic -c greeting.c -o greeting.o
9.	echo greeting.o >> ~/portfolio/.gitignore – using the file we just made put the greeting into the gitignore section
10.	echo libgreet.a >> ~/portfolio/.gitignore – same with libgreet
11.	ar rv libgreet.a greeting.o --  takes the greeting file from thee archives and then removes it
12.	gcc test_result.c -o test1 -L. -lgreet -I. – gcc allows the use of c to be compiled using github here we have taken the c file test_result.c as test1
13.	./test1 – open the test 1 file
14.	git add -A – this  adds staging rule that allows deletions etc to be set up for the next commit instead of immediately the – A flag means all files are staged
15.	git commit -m “greeting library and greeting test program” – commit a string with the given string the -m flag shows that this is the first commit to the git
16.	git push – move the commits into the main version
17.	git switch master – switch the git we are working on to the master git
18.	git branch vectors – create a new branch based of the master version
19.	git switch vectors – switch the git file we are working on to the to the vectors git version
20.	cd ~/portfolio/week3 – navigate to the given pathwayy
21.	mkdir vectors – create a new file called vectors
22.	cd vectors – navigate to this new file
23.	gcc -Wall -pedantic -c vector.c -o vector.o – gcc command is used to compile programs the wall flag enables all warnings that the gcc may throw at us. I don’t wknow what pedantic does. -c is the flag that says what file s to be compiled ad -o is the output file.
24.	ar rv libvector.a vector.o – this aacsess the archive and replaces files. The replaced files ate libvector.a with vector.o
25.	gcc test_vector_add.c -o test_vector_add1 -L. -lvector -I. – like before this uses gcc and compiles the code the output is the -o file. The -L is used to link the directory of library files and -I is used to identify header files
26.	./test_vector_add1 – open the given file
27.	git add -A – add all to be held  for commits
28.	git commit -m “code to add two vectors of fixed size”
29.	git push
30.	gcc -Wall -pedantic -c vector.c -o vector.o
31.	ar rv libvector.a vector.o
32.	gcc test_vector_dot_product.c -o test_vector_dot_product1 -L. -lvector - I.
33.	./test_vector_dot_product1
34.	git add -A
35.	git commit -m “code to calculate dot product of two vectors of fixed size”
36.	git push


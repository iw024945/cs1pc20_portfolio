#include <assert.h>
#include "vector.h"

int main(void){
int xvec[SIZ]={1,2,3};
int yvec[SIZ]={5,0,2};
int zvec[SIZ];
add_vectors(xvec,yvec,zvec);


assert(6==zvec[0]);
assert(2==zvec[1]);
assert(5==zvec[2]);
return 0;
}

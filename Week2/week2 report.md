1.	cd ~ - returns to the original directory previous worked as expected
2.	git init portfolio – I assume this creates the git directory.  I was very much correct
3.	cd portfolio – navigates to the initialised directory called portfolio
4.	ls -al  - lists every directory the -al flag lists all files (even hidden) in a long format
5.	git status – this checks if the git page is functional or not for the given directory doesn’t seem to show any information about the actual git repository
6.	echo hello > .gitignore – this writes the word hello into a file called .ignore. .ignore files are as the name suggests ignored by git and not used to push or status checks
7.	 git add -A – this adds all files to the git repository and staging area the -A flag adds everything without having to specify a directory
8.	git status – checks the status of the repository again after staging the files
9.	git config --global user.email “<student_email>”   -  this command changes the global git email to whatever email you put within the student email field
10.	git config –global user.name “<student_name>” – This command changes the global git username to whatever is within the given field
11.	git commit -m “first commit, adding week 1 content”  - This command clearly commits a comment to the git called “”first commit, adding week 1 content” a commit is basically a description of a push
12.	git status – checks the status of hit again
13.	git push – should push the file given into the repository throws error because we haven’t provided a source file to use. Also needs a remote url
14.	git remote add origin https://csgitlab.reading.ac.uk/<student_id>/cs1pc20_portfolio.git - this line adds the remote url as talked about above. The remote url is used to connect the the git repository we using to show where we are sending files
15.	git push –set-upstream origin master – shows where we are pushing too. This sets the origin master remote url as given above and the upstream is the remote branch that our local branch is tracking you can also use the -u flag
16.	git status – as above
17.	echo “# CS1PC20 Portfolio” > readme.md – this puts the words cs1pc20 protfolio into the markdown file called read me. The # makes the words appear as a title
18.	git add readme.md – adds the readme markdown file into the git repository
19.	git commit -m “added readme file” – adds what is effectively a description for the newly added markdown file so people using the git can see what it does
20.	git push – actually pushed the commits from the local repository to the remote repository making the changes global and permanent 
21.	git config –global credential.helper cache – allows you to cache your credentials so you don’t have to keep filling them in after every push/commit etc into a plain text file for some reason
22.	git branch week2  - creates a new subsection/branch called week 2 that can pushed into etc
23.	git checkout week2 – removes any added changes from the local copy of the repository useful if you no longer want to change what is in the global repository
24.	mkdir week2 – makes a new directory called week 2 from the currently in directory
25.	echo “# Week 2 exercise observations” > week2/report.md – puts tbhe given echoed text into a mark down file called week2 report . again the # before the text indicates it is a title and will be displayed as such
26.	git status – same as above but returns that it is untracked
27.	git add week2 – add the newly created week 2 file into the global/ remote repository
28.	git commit -m “added week 2 folder and report.md” – commits the description of the change we made above as the given text. Useful to allow other users to understand the change
29.	git push – pushes local commits into the remote/global repository
30.	git checkout master – navigates to the master branch, while removing any local files BUT NOT COMMITS
31.	ls -al – same as above
32.	git checkout week2 – navigates to week 2 directory
33.	ls -al – same as above but listing week2 info instead
34.	git checkout master – navigates to the master branch
35.	git merge week2 – this will merge the files in week2 with those in the master directory or whatever directory we have currently navigated too
36.	ls -al – same as above
37.	git push – pushes the given files and changes in the local branch into the remote/global repository
38.	rm -r week2 – remove everything including sub directories in the week 2 location
39.	rm -r week1 – throws an error because we have tried removing a file that does not exist as we never created week 1
40.	ls -al -  same as above
41.	git status – same as above
42.	git stash – temporarily saves changes without committing to the main branch or repository so you can work on something else. Delete on reboot
43.	git stash drop – removes the stash from its temporary holding permanently 
44.	ls -al  - same as above
45.	cd ~ - navigate to the main file
46.	cp -r portfolio portfolio_backup  - copies the portfolio and all subfolders into a file called portfolio_backup
47.	rm -rf portfolio – removes the portfolio file entirely 
48.	ls -al – lists all files to show it has been deleted
49.	git clone https://csgitlab.reading.ac.uk/<student_id>/cs1pc20_portfolio portfolio - clones the git backup back into the git repository returning it to its default space
50.	ls -al – lists all files again to show the files have returned to their default state.


```mermaid
graph TD
    StartGame((Start Game))
    InitializeGame((Initialize Game))
    MainLoop((Main Game Loop))
    LookAround((Look around))
    PrintRoomDescription((Print Room Description))
    MoveRoom((Move to another room))
    GetRoomChoice((Get Room Choice))
    MovePlayer((Move Player))
    EnterTemple((Enter Temple))
    CheckForCodeItem((Check for Code Item))
    ProvideCode((Provide Code))
    Victory((Victory!))
    EnterDen((Enter Dragon's Den))
    CheckForSwordItem((Check for Sword Item))
    KillDragon((Kill Dragon))
    StealCode((Steal Code))
    EnterMarket((Enter Goblin Market))
    GetSword((Get Sword))
    AddSwordToInventory((Add Sword to Inventory))
    PrintInventory((Print Inventory))
    BackToMainLoop((Back to Main Loop))
    Quit((Quit))
    ExamineInventory((Examine inventory))
    EndGame((End Game))
    GameOver((Game Over))

    StartGame --> InitializeGame
    InitializeGame --> MainLoop
    MainLoop --> LookAround
    LookAround --> PrintRoomDescription
    MainLoop --> MoveRoom
    MoveRoom --> GetRoomChoice
    GetRoomChoice --> MovePlayer
    MovePlayer --> PrintRoomDescription
    MovePlayer --> EnterTemple
    EnterTemple --> CheckForCodeItem
    CheckForCodeItem --> ProvideCode
    ProvideCode --> Victory
    Victory --> BackToMainLoop
    MovePlayer --> EnterDen
    EnterDen --> CheckForSwordItem
    CheckForSwordItem --> KillDragon
    KillDragon --> StealCode
    StealCode --> BackToTemple
    EnterDen --> GameOver
    MovePlayer --> EnterMarket
    EnterMarket --> GetSword
    GetSword --> AddSwordToInventory
    AddSwordToInventory --> PrintInventory
    PrintInventory --> BackToMainLoop
    MovePlayer --> Quit
    Quit --> EndGame
    MainLoop --> ExamineInventory
    ExamineInventory --> PrintInventory
    PrintInventory --> BackToMainLoop

    ```

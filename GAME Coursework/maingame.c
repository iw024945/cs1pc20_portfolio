#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

// Define structures for connections, rooms, items, players, and the game
typedef struct connection connection;
typedef struct room {
    char* name;
    char* description;
    connection* connections;
    int numConnections;
} room;

typedef struct connection {
    room* room1;
    room* room2;
} connection;

#define MAX_ITEMS 10

typedef struct {
    char* name;
    char* description;
} item;

typedef struct player {
    room* currentRoom;
    item inventory[MAX_ITEMS];
    int numItems;
} player;

typedef struct game {
    room* rooms;
    int numRooms;
    player* player;
} game;

// Function to add an item to the player's inventory
void addItem(player* player, item* itm) {
    if (player->numItems < MAX_ITEMS) {
        player->inventory[player->numItems] = *itm;
        player->numItems++;
    } else {
        printf("Inventory is full.\n");
    }
}

// Function to remove an item from the player's inventory
void removeItem(player* player, int index) {
    if (index < player->numItems) {
        for (int i = index; i < player->numItems - 1; i++) {
            player->inventory[i] = player->inventory[i + 1];
        }
        player->numItems--;
    } else {
        printf("Invalid item index.\n");
    }
}

// Function to get the player's choice from the menu
int getMenuChoice() {
    int choice;
    printf("What would you like to do?\n");
    printf("1. Look around\n");
    printf("2. Move to another room\n");
    printf("3. Quit\n");
    printf("4. Examine items in your inventory\n");
    scanf("%d", &choice);

    // Validate the choice
    while (choice < 1 || choice > 4) {
        printf("Invalid choice, please try again\n");
        scanf("%d", &choice);
    }

    return choice;
}

// Function to display the player's inventory
void examineInventory(player* player) {
    printf("You have %d items in your inventory.\n", player->numItems);
    for (int i = 0; i < player->numItems; i++) {
        printf("%d. %s: %s\n", i + 1, player->inventory[i].name, player->inventory[i].description);
    }
}

// Function to handle the Temple of the Ancients
void enterTemple(player* player) {
    bool hasCode = false;

    // Check if the player has the "Code" item in their inventory
    for (int i = 0; i < player->numItems; i++) {
        if (strcmp(player->inventory[i].name, "Code") == 0) {
            hasCode = true;
        }
    }

    if (hasCode) {
        printf("The code is 118118\n");
    }

    printf("You have entered the Temple of the Ancients. Please enter the code: ");
    char code[100];
    scanf("%s", code);

    if (strcmp(code, "118118") == 0) {
        printf("Victory! You have successfully entered the Temple of the Ancients.\n");
        exit(0);
    } else {
        printf("Game Over! The code you entered is incorrect.\n");
        exit(0);
    }
}

// Function to handle the Dragon's Den
void enterDragonsDen(player* player) {
    bool hasSword = false;

    // Check if the player has the "Sword" item in their inventory
    for (int i = 0; i < player->numItems; i++) {
        if (strcmp(player->inventory[i].name, "Sword") == 0) {
            hasSword = true;
        }
    }

    if (hasSword) {
        printf("You have entered the Dragon's Den. You have a sword so you kill the dragon and steal the code from his stash.\n");
        item itm;
        itm.name = "Code";
        itm.description = "The code to enter the Temple of the Ancients";
        addItem(player, &itm);
    } else {
        printf("You have entered the Dragon's Den. You don't have a sword so the dragon eats you and you lose the game.\n");
        exit(0);
    }
}

// Function to handle the Goblin Market
void enterMarket(player* player) {
    item itmS;
    itmS.name = "Sword";
    itmS.description = "A sharp sword";

    bool hasSword = false;

    // Check if the player already has a sword
    for (int i = 0; i < player->numItems; i++) {
        if (strcmp(player->inventory[i].name, "Sword") == 0) {
            hasSword = true;
        }
    }

    if (!hasSword) {
        // Give the player a sword
        addItem(player, &itmS);
        printf("You have entered the Goblin Market. You have been given a sword.\n");
        printf("You have %d items in your inventory.\n", player->numItems);
        for (int i = 0; i < player->numItems; i++) {
            printf("%d. %s\n", i + 1, player->inventory[i].name);
        }
    }

    // Ask the player if they want to go straight to the Dragon's Den
    printf("Would you like to go straight to the Dragon's Den?\n");
    printf("1. Yes\n");
    printf("2. No\n");
    int choice;
    scanf("%d", &choice);
    if (choice == 1) {
        enterDragonsDen(player);
    }
}

// Function to print the description of the current room
void printRoomDescription(room* room) {
    printf("You are in the %s.\n", room->name);
    printf("%s\n", room->description);
}

// Function to get the player's choice for the next room
int getRoomChoice(room* currentRoom) {
    int choice;
    printf("Which room would you like to move to?\n");
    printf("There are %d connections in this room.\n", currentRoom->numConnections);

    // Display the available connections
    for (int i = 0; i < currentRoom->numConnections; i++) {
        if (currentRoom->connections[i].room2 != NULL) {
            printf("%d. %s\n", i + 1, currentRoom->connections[i].room2->name);
        }
    }

    scanf("%d", &choice);

    // Validate the choice
    while (choice < 1 || choice > currentRoom->numConnections) {
        printf("Invalid choice, please try again\n");
        scanf("%d", &choice);
    }

    return choice;
}

// Function to move the player to the chosen room
void movePlayer(player* player, int choice) {
    player->currentRoom = player->currentRoom->connections[choice - 1].room2;
    printRoomDescription(player->currentRoom);

    // Check if the player has entered a special room
    if (strcmp(player->currentRoom->name, "Temple of the Ancients") == 0) {
        enterTemple(player);
    }
    if (strcmp(player->currentRoom->name, "Goblin Market") == 0) {
        enterMarket(player);
    }
    if (strcmp(player->currentRoom->name, "Dragon's Lair") == 0) {
        enterDragonsDen(player);
    }
    if (strcmp(player->currentRoom->name, "Haunted Manor") == 0) {
        printf("No, too scary, you run away\n and stumble across a temple\n");
        enterTemple(player);
    }
        //if player enters Mermaid Grotto
    if (strcmp(player->currentRoom->name, "Mermaid Grotto") == 0) {
        printf("You have entered the Mermaid Grotto. The mermaid whispers to you 'Visit the Goblin Market'.\n");
      
    }
}
// Function to load rooms from the "rooms.csv" and "connections.csv" files
room* loadRooms() {
    // Open the "rooms.csv" file for reading
    FILE* file = fopen("rooms.csv", "r");
    if (file == NULL) {
        printf("Error opening file\n");
        exit(1);
    }

    // Count the number of lines in the file to determine the number of rooms
    int numLines = 0;
    char line[500];
    while (fgets(line, 500, file) != NULL) {
        numLines++;
    }

    // Rewind the file to read it from the beginning
    rewind(file);

    // Allocate memory for an array of rooms based on the number of lines
    room* rooms = malloc(sizeof(room) * numLines);

    // Loop through each line in the file to populate the rooms
    for (int i = 0; i < numLines; i++) {
        fgets(line, 500, file);
        line[strlen(line) - 1] = '\0';

        // Parse the CSV data for name and description
        char* name = strtok(line, "\"");
        char* endofname = strtok(NULL, "\"");
        char* description = strtok(NULL, "\"");
        char* endofdesc = strtok(NULL, "\0");

        // Null-terminate the strings
        name[endofname - name] = '\0';

        // Create a room instance and copy the data
        room room;
        room.name = malloc(sizeof(char) * (strlen(name) + 1));
        strcpy(room.name, name);
        room.description = malloc(sizeof(char) * (strlen(description) + 1));
        strcpy(room.description, description);
        room.connections = NULL;
        room.numConnections = 0;

        // Store the room in the array
        rooms[i] = room;
    }

    // Close the file
    fclose(file);

    // Open the "connections.csv" file for reading
    FILE* file1 = fopen("connections.csv", "r");
    if (file1 == NULL) {
        printf("Error opening connections file\n");
        exit(1);
    }

    int connectionA, connectionB, connectionC;

    int currentRoomIndex = 0;

    // Loop through each line in the connections file to create connections between rooms
    while (fscanf(file1, "%d,%d,%d", &connectionA, &connectionB, &connectionC) == 3) {
        connection connection;

        // Loop through each connection in the line
        for (int j = 0; j < 3; j++) {
            // Determine the connected room index based on the connection values
            int connectedRoomIndex = (j == 0) ? connectionA : (j == 1) ? connectionB : connectionC;

            // Set up the connection between the current room and the connected room
            connection.room1 = &rooms[currentRoomIndex];
            connection.room2 = &rooms[connectedRoomIndex];

            // Check if the connection already exists to avoid duplicates
            int connectionExists = 0;
            for (int k = 0; k < rooms[currentRoomIndex].numConnections; k++) {
                if (rooms[currentRoomIndex].connections[k].room2 == &rooms[connectedRoomIndex]) {
                    connectionExists = 1;
                    break;
                }
            }

            // If the connection doesn't exist, add it to both rooms
            if (!connectionExists) {
                rooms[currentRoomIndex].connections = realloc(
                    rooms[currentRoomIndex].connections,
                    sizeof(connection) * (rooms[currentRoomIndex].numConnections + 1)
                );
                rooms[currentRoomIndex].connections[rooms[currentRoomIndex].numConnections] = connection;
                rooms[currentRoomIndex].numConnections++;

                rooms[connectedRoomIndex].connections = realloc(
                    rooms[connectedRoomIndex].connections,
                    sizeof(connection) * (rooms[connectedRoomIndex].numConnections + 1)
                );
                rooms[connectedRoomIndex].connections[rooms[connectedRoomIndex].numConnections] = connection;
                rooms[connectedRoomIndex].numConnections++;
            }
        }

        currentRoomIndex++;
    }

    // Close the connections file
    fclose(file1);

    // Return the array of rooms
    return rooms;
}

// Function to create a player with a given current room
player* createPlayer(room* currentRoom) {
    // Allocate memory for the player
    player* player = malloc(sizeof(player));
    // Set the current room and initialize the number of items
    player->currentRoom = currentRoom;
    player->numItems = 0;
    // Return the player
    return player;
}

// Function to create a game instance
game* createGame() {
    // Allocate memory for the game
    game* game = malloc(sizeof(game));
    // Load rooms and set the number of rooms
    game->rooms = loadRooms();
    game->numRooms = 10;
    // Create a player with the first room and set it in the game
    game->player = createPlayer(&game->rooms[0]);
    // Return the game instance
    return game;
}

// Function to start and control the game flow
void playGame() {
    printf("Welcome to the game\n");
    // Create a game instance
    game* game = createGame();
    // Print the description of the initial room
    printRoomDescription(game->player->currentRoom);

    // Flag to control the game loop
    bool quit = false;
    // Main game loop
    while (!quit) {
        // Get the player's choice from the menu
        int choice = getMenuChoice();

        // Process the player's choice
        if (choice == 1) {
            // Display the description of the current room again
            printRoomDescription(game->player->currentRoom);
        } else if (choice == 2) {
            // Get the player's choice for the next room and move the player
            int choice = getRoomChoice(game->player->currentRoom);
            movePlayer(game->player, choice);
        } else if (choice == 3) {
            // Set the quit flag to true, ending the game loop
            quit = true;
        } else if (choice == 4) {
            // Display the player's inventory
            examineInventory(game->player);
        }
    }
}

// Entry point of the program
int main() {
    // Start the game
    playGame();
    // Return 0 to indicate successful execution
    return 0;
}
